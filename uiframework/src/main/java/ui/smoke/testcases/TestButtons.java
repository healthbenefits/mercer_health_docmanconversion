//Author      : BHARATHISELVAN AV
//Owner       : Devanshi Desai
//Application : DocMan Conversion
//Suite       : Smoke
//TC Desc     : Verifying the button functionalities in the dashboard



package ui.smoke.testcases;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;


import pages.DashBoardPage;
import pages.GoogleHomePage;
import pages.TestButtonsPage;
import pages.DashBoard_DocmanConversion;
import utilities.InitTests;
import verify.SoftAssertions;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyElementText;
import static verify.SoftAssertions.verifyEquals;;

public class TestButtons extends InitTests {
	TestButtonsPage home;

	@Test( enabled = true) 
	public void buttontest() throws Exception {
		Driver driverFact=new Driver();
		WebDriver driver = null;
		ExtentTest test=null;
		try {
			test = reports.createTest("buttontest");
			test.assignCategory("smoke");
			driver=driverFact.initWebDriver("https://docman-qaf.mercer.com/DocManConversion/Report.aspx","IE","","","local",test,"");
			TestButtonsPage testbutton=new TestButtonsPage(driver);
			Thread.sleep(20000);

			testbutton.clickButton(TestButtonsPage.extractReport);

			String batchCtrlID = "201810245001";
			Thread.sleep(20000);
			System.out.println(batchCtrlID);
			testbutton.enterCtrlID(batchCtrlID);



			//*************Reset Button*************\\
			testbutton.clickButton(TestButtonsPage.resetbtn);
			Thread.sleep(2000);
			String batchID = TestButtonsPage.batchCtrlID.getText();
			batchID.isEmpty();

			//*************Clear Button*************\\			
			testbutton.enterCtrlID(batchCtrlID);
			testbutton.clickButton(TestButtonsPage.clearbtn);
			Thread.sleep(2000);
			String batchIDclear = TestButtonsPage.batchCtrlID.getText();
			batchIDclear.isEmpty();


			//*************Back Button*************\\	
			testbutton.clickButton(TestButtonsPage.backbtn);
			Thread.sleep(2000);
			waitForElementToDisplay(TestButtonsPage.extractReport);
			testbutton.clickButton(TestButtonsPage.extractReport);



		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("extractReport()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("extractReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();


		} 
		finally
		{
			reports.flush();
			driver.close();

		}
	}

}