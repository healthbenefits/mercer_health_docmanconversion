//Author      : BHARATHISELVAN AV
//Owner       : Devanshi Desai
//Application : DocMan Conversion
//Suite       : Smoke
//TC Desc     : Extracting Report and verifying the Trace Number with AFBO.



package ui.smoke.testcases;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;


import pages.DashBoardPage;
import pages.GoogleHomePage;
import pages.DashBoard_DocmanConversion;
import utilities.InitTests;
import verify.SoftAssertions;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyElementText;
import static verify.SoftAssertions.verifyEquals;;

public class TestExtractReport extends InitTests {
	DashBoard_DocmanConversion home;

	@Test( enabled = true) 
	public void extractReport() throws Exception {
		Driver driverFact=new Driver();
		WebDriver driver = null;
		ExtentTest test=null;
		try {
			test = reports.createTest("extractReport");
			test.assignCategory("smoke");
			driver=driverFact.initWebDriver("https://docman-qaf.mercer.com/DocManConversion/Report.aspx","IE","","","local",test,"");
			DashBoard_DocmanConversion docmanConversion=new DashBoard_DocmanConversion(driver);
			Thread.sleep(20000);

			String batchCtrlID = "201810245001";
			String str = DashBoard_DocmanConversion.Title.getText();
			System.out.println(str);
			docmanConversion.generateExtractReportID(batchCtrlID);
			System.out.println(batchCtrlID);

			verifyElementText(DashBoard_DocmanConversion.extractReportBatchID, batchCtrlID,test);

			waitForElementToDisplay(DashBoard_DocmanConversion.successCount);
			String successCount = DashBoard_DocmanConversion.successCount.getText();
			System.out.println("SuccessCount is :" + successCount);
			docmanConversion.successReportcount();

			int listofItems = DashBoard_DocmanConversion.listofItems.size();
			int listofItems1 = listofItems-1;
			System.out.println("Summary Report count is :"+ listofItems1);

			String getTraceno = DashBoard_DocmanConversion.getTracenumber.getText();

			String listofItemsstr = Integer.toString(listofItems1);
			verifyEquals(listofItemsstr,successCount, test);

			driver.get("https://afbo-qai.mercer.com/home.tpz");
			docmanConversion.verifyAFBO(getTraceno);
			String tracenumverify = DashBoard_DocmanConversion.tracenoverify.getText();

			verifyEquals(tracenumverify,getTraceno, test);

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("extractReport()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("extractReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();


		} 
		finally
		{
			reports.flush();
			driver.close();

		}
	}
}
