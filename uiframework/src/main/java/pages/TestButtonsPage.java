//Author      : BHARATHISELVAN AV
//Owner       : Devanshi Desai
//Application : DocMan Conversion
//Suite       : Smoke
//Page Name   : TestButtonsPage
//Page Desc   : All locators and methods which are used for verifying the button functionalities in the dashboard

package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.ExcelReader;

import java.io.*;
import java.util.*;


import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.setInput;

import java.util.ArrayList;
import java.util.List;

public class TestButtonsPage {
	@FindBy(xpath="//input[@value='Reset']")
	public static WebElement resetbtn;

	
	@FindBy(xpath="//input[@value='Clear']")
	public static WebElement clearbtn;
	
	@FindBy(xpath="//input[@value='Back To Report Menu']")
	public static WebElement backbtn;
	

	@FindBy(id = "cphMaster_searchControl_txtBatchControlId")
	public static WebElement batchCtrlID;
	
	@FindBy(id = "cphMaster_hlExtractReport")
	public static WebElement extractReport;

	
//****************************************************************************************\\


	public TestButtonsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void enterCtrlID (String ctrlID) {
		setInput(batchCtrlID, ctrlID);
	}


	public void clickButton(WebElement e) {
		clickElement(e);
	}
}