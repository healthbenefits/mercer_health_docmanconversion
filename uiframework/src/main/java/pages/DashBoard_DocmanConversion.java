//Author      : BHARATHISELVAN AV
//Owner       : Devanshi Desai
//Application : DocMan Conversion
//Suite       : Smoke
//Page Name   : DashBoard_DocmanConversion
//Page Desc   : All locators and methods which are used for Extracting report and verifying it.

package pages;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.ExcelReader;

import java.io.*;
import java.util.*;

import org.apache.poi.xssf.usermodel.*;

import java.io.File;
import java.io.FileFilter;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.setInput;

import java.util.ArrayList;
import java.util.List;

public class DashBoard_DocmanConversion {
	@FindBy(xpath="/html/body/form/div/h1")
	public static WebElement Title;

	@FindBy(id = "cphMaster_hlExtractReport")
	public static WebElement extractReport;

	@FindBy(id = "cphMaster_searchControl_txtFromDateTime")
	public static WebElement extractFromDate;

	@FindBy(id = "cphMaster_searchControl_txtToDateTime")
	public static WebElement extractToDate;

	@FindBy(id = "cphMaster_searchControl_txtBatchControlId")
	public static WebElement batchCtrlID;


	@FindBy(xpath = "//input[@value='Generate Extract Report']")
	public static WebElement generateExtract;

	
	@FindBy(xpath = "//input[@value='Generate Out Extract Report']")
	public static WebElement generateExtractOut;

	@FindBy(xpath = "(//h2[contains(., 'Extract Report')])[2]")
	public static WebElement extractReportHeading;

	@FindBy(xpath = "//table[@id='cphMaster_gvExtractReport']/tbody/tr/td[1]")
	public static WebElement extractReportBatchID;
	
	
	@FindBy(xpath = "//table[@id='cphMaster_gvExtractReportOut']/tbody/tr[2]/td[1]")
	public static WebElement extractOutReportBatchID;

	@FindBy(xpath = "//table[@id='cphMaster_gvExtractReport']/tbody/tr/td[1]/../td[5]/a")
	public static WebElement successCount;
	
	@FindBy(xpath = "//table[@id='cphMaster_gvExtractReportOut']/tbody/tr/td[1]/../td[5]/a")
	public static WebElement successCountExtractOut;
	
	
	////table[@id='cphMaster_gvExtractReportOut']/tbody/tr/td[1]/../td[5]/a


	@FindBy(xpath = "//table[@id='cphMaster_gvDetailedSummaryReport']/tbody/tr")
	public static List <WebElement> listofItems;
	
	
	@FindBy(xpath = "//table[@id='cphMaster_gvDetailedSummaryExtractReportOut']/tbody/tr")
	public static List <WebElement> listofItemsExtractOut;


	@FindBy(xpath = "//a/strong[contains(text(), 'Administration')]")
	public static WebElement administration;

	@FindBy(xpath = "//a[contains(text(), 'Document Management')]")
	public static WebElement documentmgmt;

	@FindBy(xpath = "//a[@href='SearchAndView.tpz?Source=AF']")
	public static WebElement searchandview;

	@FindBy(name = "ctl00$TopazWebPartManager1$tgwp1814652874$twp1814652874$txtTraceNumber")
	public static WebElement traceNumber;


	@FindBy(xpath = "//input[@value='Search' and @type='button']")
	public static WebElement search;

	@FindBy(xpath = "//table[@id='cphMaster_gvDetailedSummaryReport']/tbody/tr[2]/td[2]")
	public static WebElement getTracenumber;
	
	
	@FindBy(xpath = "//table[@id='cphMaster_gvDetailedSummaryExtractReportOut']/tbody/tr[2]/td[2]")
	public static WebElement getTracenumberExtractoutReport;
	
	


	@FindBy(xpath = "//table[@id='TopazWebPartManager1_tgwp1814652874_twp1814652874_grid']/tbody/tr[2]/td[7]")
	public static WebElement tracenoverify;


	@FindBy(xpath = "//input[@value='Export To Excel']")
	public static WebElement exportExcel;
	
	
	@FindBy(id = "cphMaster_hlExtractReportOut")
	public static WebElement extractReportOut;
	
	
	
	//****************************************************************************************\\




	public DashBoard_DocmanConversion(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}


	public void generateExtractReportDate(String fromDate, String toDate) {
		clickElement(extractReport);
		setInput(extractFromDate, fromDate);
		setInput(extractFromDate, toDate);
		clickElement(generateExtract);
	}



	public void generateExtractReportID(String controlID) {
		clickElement(extractReport);
		System.out.println("Inside func:"+controlID);
		setInput(batchCtrlID, controlID);
		clickElement(generateExtract);
	}
	public void generateExtractOutReportID(String controlID) {
		clickElement(extractReportOut);
		System.out.println("Inside func:"+controlID);
		setInput(batchCtrlID, controlID);
		clickElement(generateExtractOut);
	}	
	
	

	public void successReportcount() {
		clickElement(successCount);
	}
	
	
	public void successReportExtractOutcount() {
		clickElement(successCountExtractOut);
	}
	
	
	

	public void verifyAFBO(String traceno) {
		clickElement(administration);
		clickElement(documentmgmt);
		clickElement(searchandview);
		setInput(traceNumber, traceno);
		clickElement(search);

	}


	public void exporttoexcel () {
		clickElement(exportExcel);
	}

	public boolean isFileDownloaded(String downloadPath, String fileName) {
		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag=true;
		}

		return flag;
	}




	public void ReadDriverSuiteExcel(String sheetName, String Filepath) {
		ArrayList<String>testcaseList= new ArrayList<String>();
		XSSFWorkbook Workbook_obj = null;
		XSSFSheet sheet_obj=null;
		try{
			ClassLoader loader = new ExcelReader().getClass().getClassLoader();
			InputStream input = loader.getResourceAsStream(Filepath);

			System.out.println(input.toString());

			System.out.println("Excel file read successfully...");
			Workbook_obj = new XSSFWorkbook(input);
			System.out.println("Before reading Excel sheet...."+sheetName);
			sheet_obj = Workbook_obj.getSheet(sheetName);
			Workbook_obj.close();
			System.out.println("Excel sheet read successfully..."+sheet_obj);

			int Row_count = sheet_obj.getLastRowNum();

			System.out.print("Row count: "+Row_count+"\n");
			for (int i = 1;i<=Row_count;i++)
			{
				System.out.println("I value :"+i);
				XSSFRow row_obj = sheet_obj.getRow(i);
				XSSFCell cell_obj = row_obj.getCell(5);
				String Exec_indicator = cell_obj.getStringCellValue();
				System.out.print("Exec indicator: "+Exec_indicator+"\n");
				String Exec_ind = Exec_indicator.trim();
				System.out.println("Trim String: "+Exec_ind+"\n");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception "+e.getMessage());
		}


		//return testcaseList;
	}
	
	
	
}



